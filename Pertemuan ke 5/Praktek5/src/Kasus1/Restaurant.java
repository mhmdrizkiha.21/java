package Kasus1;

public class Restaurant {
	//read dan write harus via method maka dari itu di enkapsulasi
	private Makanan[] makanan;
	private static byte id=0;
	
	public Restaurant() {
		makanan = new Makanan[10];
	}
	
	public void tambahMenuMakanan(String nama, double harga, int stok) {
		makanan[id] = new Makanan(id, nama, harga, stok);
	}
	
	public void tampilMenuMakanan() {
		for(int i = 0 ; i<=id;i++) {
			if(!isOutOfStock(i)) {
				System.out.println(makanan[i].getId_makanan() + 
						makanan[i].getNama_makanan() + "[" + 
						makanan[i].getStok() + "]" + "\tRp. " + 
						makanan[id].getHarga_makanan());
			}
		}
	}
	
	public void pemesanan(int id, int qty) {
		if(id > this.id || id < 0 || this.isOutOfStock(id)) {
			System.out.println("Makanan tidak terdaftar");
		}else {
			if(this.makanan[id].getStok()<qty) {
				System.out.println("Stok tidak mencukupi");
			}else {				
				this.makanan[id].setStok(qty); ;
			}
		}
		
	}
	
	
	public boolean isOutOfStock(int id) {
		if(makanan[id].getStok()==0) {
			return true;
		}else {
			return false;
		}
	}
	
	public static void nextId() {
		id++;
	}

}
