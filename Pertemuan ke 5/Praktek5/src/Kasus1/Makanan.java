package Kasus1;

public class Makanan {
	private String nama_makanan; 
	private double harga_makanan;
	private int stok;
	private byte id_makanan;
	
	
	public String getNama_makanan() {
		return nama_makanan;
	}

	public void setNama_makanan(String nama_makanan) {
		this.nama_makanan = nama_makanan;
	}

	public double getHarga_makanan() {
		return harga_makanan;
	}

	public void setHarga_makanan(double harga_makanan) {
		this.harga_makanan = harga_makanan;
	}
	
	public int getStok() {
		return stok;
	}

	public void setStok(int qty) {
		this.stok -= qty;
	}

	public byte getId_makanan() {
		return id_makanan;
	}

	public void setId_makanan(byte id_makanan) {
		this.id_makanan = id_makanan;
	}
	
	public Makanan (byte id, String nama, double harga, int stok){
		this.id_makanan = id;
		this.nama_makanan = nama;
		this.harga_makanan = harga;
		this.stok = stok;
	}
	
	
	
}
