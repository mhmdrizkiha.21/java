package Kasus1;

import java.util.Scanner;

public class RestaurantMain {

	public static void main(String[] args) {
		Restaurant menu = new Restaurant();
		
		menu.tambahMenuMakanan("Bala-bala", 1_000, 20);
		Restaurant.nextId();
		
		menu.tambahMenuMakanan("Gehu", 1_000, 20);
		Restaurant.nextId();
		
		menu.tambahMenuMakanan("Tahu", 1_000, 0);
		Restaurant.nextId();
		
		menu.tambahMenuMakanan("Molen", 1_000, 20);
		
		menu.tampilMenuMakanan();
		
//		Scanner in = new Scanner(System.in);
//		
//		System.out.print("Silahkan pilih ID makanan : ");
//		int id = in.nextInt();
//		System.out.print("Jumlah makanan yang dibeli : ");
//		int qty = in.nextInt();
		System.out.println("<--------list pemesanan---------->");
		System.out.println("<--------stok makanan terbaru---------->");
		menu.pemesanan(0, 3);
		menu.pemesanan(1, 2);
		
		menu.tampilMenuMakanan();
	}
}
