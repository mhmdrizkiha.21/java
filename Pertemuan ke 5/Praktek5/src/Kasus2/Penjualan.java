package Kasus2;

public class Penjualan {
	private String[] nama_produk;
	private int[] quantity;
	private double[] harga_total;
	private static byte Id = 0;
	
	public static void nextId(){
		Id++;
	}
	//construct
	public Penjualan() {
		this.nama_produk = new String[10];
		this.quantity = new int[10];
		this.harga_total = new double[10];
	}
	//nambah item
	public void tambahItemPenjualan(String nama_produk, int quantity, double harga_total) {
		this.nama_produk[Id] = nama_produk;
		this.quantity[Id] = quantity;
		this.harga_total[Id] = harga_total;
	}
	//tampil detail pesanan
	public void tampilPesanan() {
		double total_bayar = 0;
		for(int i = 0; i < Id ; i++) {			
			System.out.println(this.nama_produk[i] + " \t" +this.quantity[i] + "\t = " + this.harga_total[i]);
			total_bayar+=this.harga_total[i];
		}
		System.out.println("Total Bayar : " + total_bayar);
	}
	
}
