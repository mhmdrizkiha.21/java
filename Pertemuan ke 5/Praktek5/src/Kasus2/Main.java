package Kasus2;

import java.util.Scanner;

public class Main {

	private static Produk[] generateProduk() {
		Produk[] list_produk = new Produk[10];
		String[] nama_produk = {
				"Batagor", "Roti Bakar", "Indomie+Telor", "Kwetiaw", "Nasi Goreng",
				"Air Mineral", "Teh Manis", "Jus Alpukat", "Teh Botol", "Kopi"
				};
		double[] harga = {
				5_000, 12_000, 10_000, 12_000, 12_0000,
				3_000, 4_000, 8_000, 5_000, 3_000
				};
		int[] qty = {
				10, 10, 20, 20, 10,
				20, 20, 10, 10, 20
				};
		for(int i = 0; i<10; i++) {
			list_produk[i] = new Produk(nama_produk[i], harga[i], qty[1]);
		}
		return list_produk;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//construct 10 produk
		Produk[] list_produk = generateProduk();
		Penjualan list_penjualan = new Penjualan();

		
		//menu produk
		System.out.println("Daftar Menu Makanan");
		System.out.println("==============================");
		for(int i = 0; i<10; i++) {
			list_produk[i].print();
		}
		
		//pembelian
		Scanner in = new Scanner(System.in);
		char lagi = 'n';
		byte count_pembelian = 1;
		do {
			System.out.print("Silahkan pilih ID makanan : ");
			int id = in.nextInt();
			System.out.print("Jumlah makanan yang dibeli : ");
			int qty_pembelian = in.nextInt();
			String nama_produk = list_produk[id-1].getNama_produk();
			double harga = list_produk[id-1].getHarga();
			list_penjualan.tambahItemPenjualan(nama_produk, qty_pembelian, qty_pembelian * harga);
			list_penjualan.nextId();
			System.out.println("apakah akan memesan lagi ? (Y/N)");
			lagi = in.next().charAt(0);
			count_pembelian++;
		}while((lagi=='y'||lagi=='Y') && count_pembelian <= 10);
		
		list_penjualan.tampilPesanan();
	}

}
