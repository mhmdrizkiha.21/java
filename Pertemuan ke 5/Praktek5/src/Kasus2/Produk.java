package Kasus2;

public class Produk {
	private String nama_produk; 
	private double harga;
	private int qty;
	private int id;
	private static int nextId;
	
	//menambahkan id ketika class di initiasi
	{
		id = nextId;
		nextId++;
	}
	//setter dan getter
	public String getNama_produk() {
		return nama_produk;
	}
	public void setNama_produk(String nama_produk) {
		this.nama_produk = nama_produk;
	}
	public double getHarga() {
		return harga;
	}
	public void setHarga(double harga) {
		this.harga = harga;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public int getId() {
		return id;
	}
	//construct
	public Produk (String nama, double harga, int qty){
		this.nama_produk = nama;
		this.harga = harga;
		this.qty = qty;
	}
	//print produk
	public void print() {
		System.out.println(this.id + 1 + ". " + this.nama_produk + " = Rp. " + this.harga);
	}
}
