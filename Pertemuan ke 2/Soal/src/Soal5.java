import java.util.Scanner;

public class Soal5 {

	public static void main(String[] args) {
		//read string A dan B		
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
	    String A = myObj.nextLine();  // Read user input
	    String B = myObj.nextLine();  // Read user input
	    //mengubah huruf depan menjadi uppercase
	    A = A.substring(0,1).toUpperCase() + A.substring(1);
	    B = B.substring(0,1).toUpperCase() + B.substring(1);
	    // compare untuk keperluan penentuan lexico
	    String compare = (A.substring(0,1).compareTo(B.substring(0,1)) > 0)?"yes":"no";
	    // print hasil
	    System.out.println(A.length() + B.length());
	    System.out.println(compare);
	    System.out.println(A + " " + B);
	}

}
