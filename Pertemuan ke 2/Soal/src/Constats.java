
public class Constats {

	public static void main(String[] args) {
		final double CM_PER_INCH = 2.54; //mendfinisikan var yang tidak akan bisa dirubah (constraint)
		//mendefinisikan var dengan isian value bertipe data double
		double paperWidth = 8.5; 
		double paperHeight = 11; //mengisi double dengan tipe data integer
		System.out.println("Paper size in centimeters: " + 
		paperWidth * CM_PER_INCH + " by " + paperHeight * CM_PER_INCH);
	}

}
