package Task3;

/**
* @author Muhammad Rizki Halomoan
*/
public class WeeklySales {
	public static void main(String[] args)
	{
		 Salesperson [] salesStaff = new Salesperson [4];
		 salesStaff[0] = new Salesperson ("Rizki", "Halohalo", 12000);
		 salesStaff[1] = new Salesperson ("Benny", "Yoga", 10000);
		 salesStaff[2] = new Salesperson ("Nuval", "Ardana", 9000);
		 salesStaff[3] = new Salesperson ("Rizki", "Halomoan", 12000);
		 Sorting.insertionSort(salesStaff);
		 System.out.println("\nRanking of Sales for the week\n");
		 for(Salesperson s : salesStaff)
		 System.out.println(s);
	}
}
