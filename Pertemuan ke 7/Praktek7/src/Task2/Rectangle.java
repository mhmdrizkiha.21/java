package Task2;

/**
* @author Muhammad Rizki Halomoan
*/

public class Rectangle extends Shape{

	private double length;
	private double width;
	public Rectangle(double length, double width) {
		super("Rectangle");
		this.length = length;
		this.width = width;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return width * length;
	}
	
	public String toString()
	{
		return super.toString() + "of length " + length+ " and of width " + length;
	}

}
