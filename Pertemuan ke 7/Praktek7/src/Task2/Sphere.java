package Task2;

/**
* @author Muhammad Rizki Halomoan
*/

public class Sphere extends Shape{
	private double radius;
	
	public Sphere(double r)
	{
		super("Sphere");
		radius = r;
	}
	@Override
	public double area() 
	{
		// TODO Auto-generated method stub
		return 4 * Math.PI*radius*radius;
	}
	
	public String toString()
	{
		return super.toString() + " of radius " + radius;
	}
	
}
