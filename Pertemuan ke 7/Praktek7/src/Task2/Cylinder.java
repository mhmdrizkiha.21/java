package Task2;

/**
* @author Muhammad Rizki Halomoan
*/

public class Cylinder extends Shape{

	private double radius;
	private double height;
	public Cylinder(double radius, double height) {
		super("Cylinder");
		// TODO Auto-generated constructor stub
		this.radius = radius;
		this.height = height;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return Math.PI*radius*radius*height;
	}
	
	public String toString()
	{
		return super.toString() + "of radius " + radius + " and of height " + height;
	}

}
