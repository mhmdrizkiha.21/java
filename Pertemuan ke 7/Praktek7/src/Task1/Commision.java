package Task1;

/**
* @author Muhammad Rizki Halomoan
*/

public class Commision extends Hourly{
	private double sales;
	private double commisionRate;

	public Commision(String eName, String eAddress, String ePhone, String socSecNumber, 
			double rate, double commisionRate) {
		super(eName, eAddress, ePhone, socSecNumber, rate);
		// TODO Auto-generated constructor stub
		this.commisionRate = commisionRate;
		sales = 0;
	}
	
	public void addSales(double totalSales) {
		sales+=totalSales;
	}
	
	public double pay() {
		double payment = commisionRate * sales + super.pay();
		sales = 0;
		
		return payment;
	}
	
	public String toString() {
		String result = super.toString();
		result += "\nCurrent totalSales: " + sales;
		
		return result;
	}

}
