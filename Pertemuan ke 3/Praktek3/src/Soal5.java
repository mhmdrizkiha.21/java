import java.util.Scanner;

/**
 * <h1>Buka Tutup Jalan</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */

public class Soal5 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String a = in.next() + in.next() + in.next() + in.next();
		long a_long = Long.parseLong(a);
		int hasil_mod = (int)((a_long-999999)%5);
		
		String status = (hasil_mod==0)?"jalan":"berhenti";
		System.out.println(status);	
	}

}
