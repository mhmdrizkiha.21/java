import java.util.Arrays;
import java.util.Scanner;

/**
 * <h1>Input and Output(2)</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */
public class Soal2 {

	public static void main(String[] args) {
		//melakukan scan input
		Scanner in = new Scanner(System.in);
		//menangkap 3 inputan
		String [][] store = new String[3][];
		
		//split inputan dan kirim ke variable store
		for(int i=0; i<3; i++) {
			String inputan = in.nextLine();
			store[i]= inputan.split(" ");
		}
		
		System.out.println("================================"); 
		for(String[] operasi:store) {
			if((operasi[0].length()<=10) && (Integer.parseInt(operasi[1]) >= 0 && Integer.parseInt(operasi[1])<=999) ) {
				System.out.printf("%-15s", operasi[0]);
				System.out.printf("%03d\n", Integer.parseInt(operasi[1]));
			}else{
				System.out.println("Inputan tidak Sesuai");
			}
		}
		System.out.println("================================");
		
	}

}
