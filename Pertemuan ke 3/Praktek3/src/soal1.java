import java.util.Scanner;

/**
 * <h1>Input and Output!</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */

public class soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		String[] output = inputan.split("[\\s,!?._'@]");
		System.out.println(output.length);
		for(String out:output) {			
			System.out.println(out);
		}
	}
}
