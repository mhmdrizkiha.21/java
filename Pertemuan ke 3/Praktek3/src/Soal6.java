import java.util.Scanner;
import java.math.BigInteger;

/**
 * <h1>Big Number</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */


public class Soal6 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		BigInteger a = new BigInteger(in.next()); //inputan a
		BigInteger b = new BigInteger(in.next()); //inputan b
		
		System.out.println(a.add(b));
		System.out.println(a.multiply(b));
	}

}
