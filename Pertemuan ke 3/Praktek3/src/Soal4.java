import java.util.Scanner;

/**
 * <h1>Gaji Agent</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */

public class Soal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int penjualan = in.nextInt(); //inputan penjualan
		
		int harga_item = 50000;
		double bonus_item = (penjualan>=15)?0.1 * harga_item * penjualan:0;
		
		int gaji_pokok = 500000;
			double bonus_penjualan = (penjualan > 80)?0.35*(penjualan * harga_item):
										(penjualan >=40)?0.25*(penjualan * harga_item):0;
		double denda = (penjualan < 15)?0.15 * (harga_item * (15 - penjualan)):0;
		
		
		int gajih =(int)(gaji_pokok + bonus_penjualan + bonus_item - denda);
		
		System.out.println(gajih);
	}

}
