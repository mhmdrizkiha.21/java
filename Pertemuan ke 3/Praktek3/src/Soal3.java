import java.util.Scanner;

/**
 * <h1>Berhitung</h1>
 * 	
 * @author Muhammad Rizki Halomoan
 * @version 1.0
 * @since 2022-09-05
 */

public class Soal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		String[] operasi = inputan.split(" ");
		int A  = Integer.parseInt(operasi[0]);
		int B  = Integer.parseInt(operasi[2]);
		int hasil = 0;
		if(A>=1 && A<=1000 && B>=1 && B<=1000) {
			if(operasi[1].equals("+")) {
				hasil = A + B;
			}else if(operasi[1].equals("-")) {
				hasil = A - B;
			}else if(operasi[1].equals("*")) {
				hasil = A * B;
			}else if(operasi[1].equals("%")) {
				hasil = A % B;
			}else if(operasi[1].equals("/")) {
				if(A % B != 0) {
					System.out.println("A dan B tidak habis dibagi	");
				}else {					
					hasil = A / B;
				}
			}else {
				System.out.println("operator tidak terdaftar");
			}
				
			System.out.println(hasil); 
		}else {
			System.out.println("nilai diluar jangkauan");
		}
		
	}

}
